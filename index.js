require("dotenv").config()
const processOutages = require("./process")
const scrape = require("./scrape")
const tweet = require("./tweet")

const interval = process.env.REFRESH_INTERVAL_MIN || 5

function main() {
  scrape()
    .then(rows => processOutages(rows))
    .then(outages =>
      outages.map(o =>
        tweet(o).catch(err => {
          console.error(">>> ERROR:", err)
        })
      )
    )
    .catch(err => {
      console.error(">>> ERROR:", err)
    })
}

// wait before kicking off first job
// so that any stored data is loaded
setTimeout(() => main(), 1000)

// set scrape cadence
setInterval(main, interval * 1000 * 60)
