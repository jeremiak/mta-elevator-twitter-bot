const cheerio = require("cheerio")
const http = require("axios")
const trim = require("lodash.trim")

const colMapping = [
  "station_name",
  "lines",
  "number",
  "location",
  "out_of_service",
  "reason",
  "estimated_return"
]

const parseRow = $row => {
  const $tds = $row.find("td")
  if ($tds.length === 0) return null
  const row = {}
  $tds.each((i, elem) => {
    const $elem = cheerio(elem)
    if (i === 1) {
      row.lines = $elem
        .find("img")
        .map((ii, imgElem) => cheerio(imgElem).attr("alt")[0])
        .toArray()
    } else if (i !== 7) {
      const text = $elem.text()
      const key = colMapping[i]
      row[key] = trim(text)
    }
  })
  return row
}

const fetchAndParse = () => {
  const url =
    "http://advisory.mtanyct.info/EEoutage/EEOutageReport.aspx?StationID=All"

  console.log(">>> scraping")

  return http.get(url).then(response => {
    const $ = cheerio.load(response.data)
    const $table = $("#ctl00_ContentPlaceHolder1_gvElevator")
    const $rows = $table.find("tr")

    const rows = $rows.map((i, elem) => parseRow($(elem))).toArray()
    console.log(`>>> ${rows.length} outages found`)
    return rows
  })
}

module.exports = fetchAndParse
