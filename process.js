const fs = require("fs")
const path = require("path")

const dbPath = path.join(__dirname, ".data.json")
let db = {}

fs.exists(dbPath, exists => {
  if (!exists) return
  console.log('>>> saved db found, loading data')
  const dbFile = fs.readFileSync(dbPath)
  db = JSON.parse(dbFile.toString())
  console.log(`>>> loaded ${Object.keys(db).length} outages`)
})

const getOutageKey = outage => `${outage.number}--${outage.out_of_service}`

const filterOnlyNewOutages = outages => {
  console.log(">>> filtering outages for new ones only")

  const onlyNew = outages.filter(outage => {
    const id = getOutageKey(outage)
    if (!db[id]) {
      db[id] = true
      return true
    }
    return false
  })

  console.log(`>>> of ${outages.length} outages, ${onlyNew.length} are new`)

  return onlyNew
}

setInterval(() => {
  const keys = Object.keys(db)
  console.log(`>>> there are currently ${keys.length} outages in db`)
}, 2 * 1000 * 60)

process.on("SIGINT", () => {
  const json = JSON.stringify(db)
  fs.writeFile(dbPath, json, () => {
    process.exit(1)
  })
})

module.exports = filterOnlyNewOutages
