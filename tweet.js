require("dotenv").config()
const Twitter = require("twitter")
// const http = require("http")

const client = new Twitter({
  consumer_key: process.env.TWITTER_CONSUMER_KEY,
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
  access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
})

const createTweetText = outage => {
  const lines = outage.lines.map(l => `#${l}`).join(" ")
  const text = `An elevator just went out of service at the ${
    outage.station_name
  } station, which serves the ${lines} lines. It is the one that is on the ${outage.location.toLowerCase()}.`

  return text.replace(/\slirr\s/g, ' LIRR ')
}

const postTweet = outage => {
  const status = createTweetText(outage)
  return new Promise((resolve, reject) => {
    console.log(`>>> tweeting: ${status}`)
    client.post("statuses/update", { status }, (error, tweet, response) => {
      if (error) return reject(error)

      resolve(response)
    })
  })
}

module.exports = postTweet
