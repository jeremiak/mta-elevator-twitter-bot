# MTA elevator outage Twitter bot

Scrapes data from the [MTA](http://advisory.mtanyct.info/EEoutage/EEOutageReport.aspx?StationID=All) and tweets about new outages

## Commands

`npm start` - start the app using `pm2`
`npm stop` - stop the app
`npm run logs` - show logs for the app

If you have `pm2` installed globally (`npm install -g pm2`) you can just use `pm2` to run and manage the app if you want.